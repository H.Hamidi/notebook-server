package com.notebook.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.notebook.dto.Input;
import com.notebook.dto.Response;
import com.notebook.exception.NotebookServerException;
import com.notebook.model.Code;
import com.notebook.service.IExecuteService;
import com.notebook.util.FormateCode;

/**
 * 
 * @author Hafid
 *
 */
@RestController
public class ExecuteController {

	@Autowired
	private IExecuteService executeService;

	@PostMapping(value = "/execute")
	public Response execute(@RequestBody Input input,
			@RequestHeader(name = "sessionId", required = false, defaultValue = "anonymous") String user)
			throws IOException, InterruptedException
	{
		try 
		{
			Code code = FormateCode.parseCode(input.getCode());
			String resutl = executeService.execute(code, user);
			return new Response(resutl);
		} catch (NotebookServerException e) 
		{
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}

	}

}
