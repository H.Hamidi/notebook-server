package com.notebook.model;

/**
 * 
 * @author Hafid
 *
 */
public class Code {

	private String code;
	private Interpreter interpreter;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Interpreter getInterpreter() {
		return interpreter;
	}

	public void setInterpreter(Interpreter interpreter) {
		this.interpreter = interpreter;
	}

	@Override
	public String toString() {
		return "Code [code=" + code + ", interpreter=" + interpreter + "]";
	}

}
