package com.notebook.model;

/**
 * 
 * @author Hafid
 *
 */
public enum Interpreter {
	PYTHON("python", ".py", "python");

	private String name;
	private String extension;
	private String command;

	Interpreter(String name, String extension, String command) {
		this.name = name;
		this.extension = extension;
		this.command = command;
	}

	public String getName() {
		return name;
	}

	public String getExtension() {
		return extension;
	}

	public String getCommand() {
		return command;
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static Interpreter from(String name) {
		for (Interpreter interpreter : Interpreter.values()) {
			if (interpreter.name.equalsIgnoreCase(name))
				return interpreter;
		}
		return null;
	}

}
