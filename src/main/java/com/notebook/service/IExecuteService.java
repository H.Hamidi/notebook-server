package com.notebook.service;

import com.notebook.exception.CommandRunnerTimeOutException;
import com.notebook.exception.UnsupportedInterpeterException;
import com.notebook.model.Code;
/**
 * 
 * @author Hafid
 *
 */
public interface IExecuteService {
	
	/**
	 * 
	 * @param code
	 * @param user
	 * @return
	 * @throws CommandRunnerTimeOutException
	 * @throws UnsupportedInterpeterException
	 */
	public String execute(Code code, String user) throws CommandRunnerTimeOutException, UnsupportedInterpeterException;
	

}
