package com.notebook.service;

import java.io.File;
import java.io.IOException;

import org.springframework.stereotype.Service;

import com.notebook.exception.CommandRunnerTimeOutException;
import com.notebook.exception.NoteBookServerRunTimeException;
import com.notebook.exception.UnsupportedInterpeterException;
import com.notebook.model.Code;
import com.notebook.model.Interpreter;
import com.notebook.util.CommandRunner;
import com.notebook.util.FileScript;

/**
 * 
 * @author Hafid
 *
 */
@Service
public class ExecuteService implements IExecuteService {

	private CommandRunner commandRunner;
	// private File sourceCode;

	@Override
	public String execute(Code code, String user) throws UnsupportedInterpeterException, CommandRunnerTimeOutException {
		// TODO Auto-generated method stub
		String result = "";
		if(isNull(code,user))
		{
			throw new NoteBookServerRunTimeException("code and user parametre may not be null ");
		}
		if (Interpreter.from(code.getInterpreter().getName()) == null) {
			throw new UnsupportedInterpeterException("unsupported interpreter " + code.getInterpreter().getName());
		}
		File script = FileScript.getFileScript(user, code.getInterpreter().getExtension(), code.getCode());
		Runtime rt = Runtime.getRuntime();
		Process pr;
		try {
			pr = rt.exec(code.getInterpreter().getCommand().concat(" ").concat(script.getAbsolutePath()));
			commandRunner = new CommandRunner(pr);
			commandRunner.run();
			String error = commandRunner.getError();
			if (!error.isEmpty()) {
				throw new NoteBookServerRunTimeException(error);
			}
			return commandRunner.getResult();

		} catch (CommandRunnerTimeOutException e) {
			commandRunner.getProcess().destroyForcibly();
			script.delete();
			throw new NoteBookServerRunTimeException(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private boolean isNull(Code code, String user)
	{
		if(user == null) return true;
		if(code == null || code.getInterpreter()== null || code.getCode() == null) return true;
		return false;
		
	}
}
