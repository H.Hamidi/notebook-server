package com.notebook.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.notebook.exception.InputFormatException;
import com.notebook.exception.UnsupportedInterpeterException;
import com.notebook.model.Code;
import com.notebook.model.Interpreter;
/**
 * 
 * @author Hafid
 *
 */
public class FormateCode {

	private static final String REGX = "(^%)(\\w+)(\\s)(.+)";
	private static final String FORMAT = "%<interpreter-name><whitespace><code>";

	/**
	 * 
	 * @param input
	 * @return
	 * @throws InputFormatException
	 * @throws UnsupportedInterpeterException
	 */
	static public Code parseCode(String input) throws InputFormatException, UnsupportedInterpeterException {

		Code code = new Code();
		Pattern pattern = Pattern.compile(REGX);
		Matcher matcher = pattern.matcher(input);
		if (!matcher.matches()) 
		{
			throw new InputFormatException("can not parse the input, please respect the following format : " + FORMAT);
		}
		else 
		{
			String interpreterName = matcher.group(2);
			Interpreter interpreter = Interpreter.from(interpreterName);
			if (interpreter == null) {
				throw new UnsupportedInterpeterException("unsupported interpreter " + interpreterName);
			}
			String codeFromInput = matcher.group(4);
			code.setCode(codeFromInput);
			code.setInterpreter(interpreter);
		}
		return code;
	}
}
