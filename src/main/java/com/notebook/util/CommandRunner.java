package com.notebook.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

import com.notebook.exception.CommandRunnerTimeOutException;
/**
 * 
 * @author Hafid
 *
 */
public class CommandRunner{
	
	private static final long MAX_WAIT = 20;
	
	private Process process;
	private String result;
	private String error;

	public CommandRunner(Process process)
	{
		this.process = process;
	}
	
	public void run() throws CommandRunnerTimeOutException {
		
		try { 
		      boolean exitVal = process.waitFor(MAX_WAIT, TimeUnit.SECONDS);
				if(!exitVal)
			     {
					process.destroy();
			    	 throw new CommandRunnerTimeOutException("your code take match time than exepected ("+MAX_WAIT+" s)");
			     }
				this.error = outPut(process, process.getErrorStream());
			    this.result = outPut(process, process.getInputStream());
		    } catch (InterruptedException e) {
		    	e.printStackTrace();
		    }
		
	}
	
	private String outPut(Process pr, InputStream is){
		 BufferedReader in = new BufferedReader(new InputStreamReader(is));
	      String line;
	      String result = "";
			try {
				while ((line = in.readLine()) != null) {
					 result = result.concat(line + " ");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		return result.trim();
		
	}

	public String getResult() {
		return result;
	}

	public String getError() {
		return error;
	}

	public Process getProcess() {
		return process;
	}


}
