package com.notebook.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 * @author Hafid
 *
 */
public class FileScript {
	
	private static final String LINE_SEPQRATOR = "line.separator";

	public static File getFileScript(String name, String extension, String contenu)
	{
		File file = new File(name.concat(extension));
		FileWriter myWriter;
		try {
			myWriter = new FileWriter(file, true);
			myWriter.write(contenu);
			myWriter.write(System.getProperty(LINE_SEPQRATOR));
			myWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file;
	}
	
}
