package com.notebook.dto;

public class Response {

	private String result;

	public Response() {
	}

	public Response(String result) {
		super();
		this.result = result;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
	
	
}
