package com.notebook.exception;

/**
 * 
 * @author Hafid
 *
 */
public class NoteBookServerRunTimeException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6883905546996730828L;
	
	public NoteBookServerRunTimeException() {
        super();
    }
  
    public NoteBookServerRunTimeException(String message) {
        super(message);
    }
}
