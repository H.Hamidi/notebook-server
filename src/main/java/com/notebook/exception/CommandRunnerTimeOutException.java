package com.notebook.exception;

/**
 * 
 * @author Hafid
 *
 */
public class CommandRunnerTimeOutException extends NotebookServerException  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -141155854325073062L;

	public CommandRunnerTimeOutException() {
        super();
    }
  
    public CommandRunnerTimeOutException(String message) {
        super(message);
    }
}
