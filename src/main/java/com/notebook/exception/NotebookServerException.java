package com.notebook.exception;

/**
 * 
 * @author Hafid
 *
 */

public class NotebookServerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1943242640165849618L;
	
	
	  public NotebookServerException() {
	        super();
	    }
	    public NotebookServerException(String message, Throwable cause) {
	        super(message, cause);
	    }
	    public NotebookServerException(String message) {
	        super(message);
	    }

}
