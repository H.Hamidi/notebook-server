package com.notebook.exception;

/**
 * 
 * @author Hafid
 *
 */
public class InputFormatException extends NotebookServerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7056639268226582229L;
	
	public InputFormatException() {
        super();
    }
  
    public InputFormatException(String message) {
        super(message);
    }



}
