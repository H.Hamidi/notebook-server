package com.notebook.exception;

/**
 * 
 * @author Hafid
 *
 */
public class UnsupportedInterpeterException extends NotebookServerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8069606267884895667L;
	

	  public UnsupportedInterpeterException() {
	        super();
	    }
	  
	    public UnsupportedInterpeterException(String message) {
	        super(message);
	    }


}
